package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// сюда писать код

func ExecutePipeline(jobs ...job) {
	in := make(chan interface{})
	wg := &sync.WaitGroup{}

	for _, j := range jobs {
		wg.Add(1)
		out := make(chan interface{})
		go worker(in, out, j, wg)
		in = out
	}

	wg.Wait()
}

func worker(in, out chan interface{}, job job, wg *sync.WaitGroup) {
	job(in, out)
	close(out)
	wg.Done()
}

func toString(value interface{}) string {
	switch value.(type) {
	case int:
		return strconv.Itoa(value.(int))
	case string:
		return value.(string)
	default:
		panic(fmt.Sprintf("can't convert type %T to string", value))
	}
}

func crc32Signer(data string, out chan<- string) {
	out <- DataSignerCrc32(data)
}

func SingleHash(in, out chan interface{}) {
	mu := &sync.Mutex{}
	wg := &sync.WaitGroup{}
	for v := range in {
		wg.Add(1)
		go func(v interface{}, out chan interface{}, mu *sync.Mutex, wgHash *sync.WaitGroup) {
			data := toString(v)
			mu.Lock()
			md5Hash := DataSignerMd5(data)
			mu.Unlock()

			outCRC32 := make(chan string)
			outCRC32MD5 := make(chan string)
			go crc32Signer(data, outCRC32)
			go crc32Signer(md5Hash, outCRC32MD5)

			out <- <-outCRC32 + "~" + <-outCRC32MD5
			wgHash.Done()
		}(v, out, mu, wg)
	}
	wg.Wait()
}

func MultiHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for v := range in {
		wg.Add(1)
		go func(v interface{}, out chan interface{}, wgHash *sync.WaitGroup) {
			data := toString(v)
			multiHashData := make([]string, 6)
			mu, wg := &sync.Mutex{}, &sync.WaitGroup{}

			for i := 0; i <= 5; i++ {
				wg.Add(1)
				go func(data string, idx int, multiHash []string, mu *sync.Mutex, wg *sync.WaitGroup) {
					data = DataSignerCrc32(strconv.Itoa(idx) + data)
					mu.Lock()
					multiHash[idx] = data
					mu.Unlock()
					wg.Done()
				}(data, i, multiHashData, mu, wg)
			}

			wg.Wait()
			out <- strings.Join(multiHashData, "")
			wgHash.Done()
		}(v, out, wg)
	}
	wg.Wait()
}

func CombineResults(in, out chan interface{}) {
	var hashes []string

	for v := range in {
		hashes = append(hashes, v.(string))
	}

	sort.Strings(hashes)
	out <- strings.Join(hashes, "_")
}
